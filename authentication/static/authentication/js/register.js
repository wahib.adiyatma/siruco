$("#btn-role").click(e => {
  e.preventDefault()
  const role = $('select[name="role"]').val();
  if (role != $("#btn-back").data("active-role")) {
    $("#choose-role").toggleClass("d-none");
    $("#bottom-button").toggleClass("d-none")
    $("#bottom-button").toggleClass("d-flex")
    switch (role) {
      case "adminsistem":
        $("#role-form").html(adminsistem);
        $("#btn-back").data("active-role", "adminsistem");
        break;
      case "adminsatgas":
        $("#role-form").html(adminsatgas);
        $("#btn-back").data("active-role", "adminsistem");
        break;
      case "penggunapublik":
        $("#role-form").html(penggunapublik);
        $("#btn-back").data("active-role", "penggunapublik");
        break;
      case "dokter":
        $("#role-form").html(dokter);
        $("#btn-back").data("active-role", "dokter");
        break;
      default:
        break;
    }
  }
});
$("#btn-back").click((e) => {
  e.preventDefault();
  $("#choose-role").toggleClass("d-none");
  $("#role-form").html("");
  $("#bottom-button").toggleClass("d-none")
  $("#bottom-button").toggleClass("d-flex")
});

const adminsistem = `
<h5 class="text-center mb-3">Masukkan data diri Anda</h5>
<div class="form-group">
  <label for="email">Email</label>
  <input
    type="email"
    name="email"
    class="form-control"
    id="email"
    placeholder="Email"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="password">Password</label>
  <input
    type="password"
    name="password"
    class="form-control"
    id="password"
    placeholder="Password"
    maxlength="50"
    required
  />
</div>  
`

const adminsatgas = `
<h5 class="text-center mb-3">Masukkan data diri Anda</h5>
<div class="form-group">
  <label for="email">Email</label>
  <input
    type="email"
    name="email"
    class="form-control"
    id="email"
    placeholder="Email"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="password">Password</label>
  <input
    type="password"
    name="password"
    class="form-control"
    id="password"
    placeholder="Password"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="gender">Jenis Kelamin</label>
  <select name="gender" id="gender" class="form-control" required>
    <option selected disabled>Pilih...</option>
    <option value="M">Pria</option>
    <option value="F">Wanita</option>
  </select>
</div>  
`

const penggunapublik = `
<h5 class="text-center mb-3">Masukkan data diri Anda</h5>
<div class="form-group">
  <label for="email">Email</label>
  <input
    type="email"
    name="email"
    class="form-control"
    id="email"
    placeholder="Email"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="password">Password</label>
  <input
    type="password"
    name="password"
    class="form-control"
    id="password"
    placeholder="Password"
    maxlength="50"
    required
  />
</div>  
<div class="form-group">
  <label for="name">Nama Lengkap</label>
  <input
    type="text"
    name="name"
    class="form-control"
    id="name"
    placeholder="Nama lengkap"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="hp">NIK</label>
  <input
    type="text"
    name="nik"
    class="form-control"
    id="nik"
    placeholder="NIK"
    maxlength="40"
    required
  />
</div>
<div class="form-group">
  <label for="hp">Nomor Telepon</label>
  <input
    type="text"
    name="hp"
    class="form-control"
    id="hp"
    placeholder="Nomor Telepon"
    maxlength="20"
    required
  />
</div>
`
const dokter = `
<h5 class="text-center mb-3">Masukkan data diri Anda</h5>
<div class="form-group">
  <label for="email">Email</label>
  <input
    type="email"
    name="email"
    class="form-control"
    id="email"
    placeholder="Email"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="password">Password</label>
  <input
    type="password"
    name="password"
    class="form-control"
    id="password"
    placeholder="Password"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="name">No. STR</label>
  <input
    type="text"
    name="nostr"
    class="form-control"
    id="nostr"
    placeholder="No STR"
    maxlength="50"
    required
  />
</div>  
<div class="form-group">
  <label for="name">Nama Lengkap</label>
  <input
    type="text"
    name="name"
    class="form-control"
    id="name"
    placeholder="Nama lengkap"
    maxlength="50"
    required
  />
</div>
<div class="form-group">
  <label for="hp">Nomor Telepon</label>
  <input
    type="text"
    name="hp"
    class="form-control"
    id="hp"
    placeholder="Nomor Telepon"
    maxlength="20"
    required
  />
</div>

<div class="form-group">
  <label for="date">Gelar Depan</label>
  <input
    type="text"
    name="gelardepan"
    class="form-control"
    id="gelardepan"
    placeholder="Gelar Depan"
    required
  />
</div>
<div class="form-group">
  <label for="date">Gelar Belakang</label>
  <input
    type="text"
    name="gelarbelakang"
    class="form-control"
    id="gelarbelakang"
    placeholder="Gelar Belakang"
    required
  />
</div>
`