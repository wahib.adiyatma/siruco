from django.db.utils import IntegrityError
from django.shortcuts import redirect, render
from django.db import connection

# Create your views here.
def index(request):
    return render(request, "authentication/index.html")

def login(request):
    return render(request,"authentication/login.html")

def register(request):
    context = {}
    if request.method == "POST":
        email = request.POST.get("email")
        password = request.POST.get("password")
        name = request.POST.get("name")
        hp = request.POST.get("hp")
        gender = request.POST.get("gender")
        address = request.POST.get("address")
        if request.POST.get("role") == "pelanggan":
            date = request.POST.get("date")
            no_va = ""
            with  connection.cursor() as c:
                # mengambil nomor virtual account terbesar terakhir
                c.execute("select max(no_virtual_account) from pelanggan")
                row = c.fetchone()
                # jika table pelanggan sudah ada isinya
                if row[0] != None:
                    no_va = str(int(row[0]) + 1)
                # jika table pelanggan masih kosong
                else:
                    no_va = "100000001"
                # insert pelanggan baru
                try:
                    pengguna = (email, password, name, address, hp, gender)
                    pelanggan = (email, no_va, 0, date)
                    c.execute(f"insert into pengguna values {str(pengguna)}", )
                    c.execute(f"insert into pelanggan values {str(pelanggan)}")
                    request.session["user_email"] = email
                    request.session["user_role"] = request.POST.get("role")
                    return redirect("authentication:index")
                # jika sudah ada email terdaftar maka failed
                except IntegrityError:
                    context["error"] = "email sudah terdaftar"
                    return render(request, 'authentication/register.html', context)
        if request.POST.get("role") == "staff" or request.POST.get("role") == "kurir":
            npwp = request.POST.get("npwp")
            norek = request.POST.get("norek")
            bank = request.POST.get("bank")
            cabang = request.POST.get("cabang")
            # jika staf
            if request.POST.get("role") == "staff":
                with connection.cursor() as c:
                    # insert staf baru
                    try:
                        pengguna = (email, password, name, address, hp, gender)
                        staf = (email, npwp, norek, bank, cabang)
                        c.execute(f"insert into pengguna values {str(pengguna)}", )
                        c.execute(f"insert into staf values {str(staf)}")
                        request.session["user_email"] = email
                        request.session["user_role"] = request.POST.get("role")
                        return redirect("authentication:index")
                    # jika sudah ada email terdaftar maka failed
                    except IntegrityError:
                        context["error"] = "email sudah terdaftar"
                        return render(request, 'authentication/register.html', context)
            elif request.POST.get("role") == "kurir":
                sim = request.POST["sim"]
                nokendaraan = request.POST["nokendaraan"]
                jeniskendaraan = request.POST["jeniskendaraan"]
                with connection.cursor() as c:
                    # insert kurir baru
                    try:
                        pengguna = (email, password, name, address, hp, gender)
                        kurir = (email, npwp, norek, bank, cabang, sim, nokendaraan, jeniskendaraan)
                        c.execute(f"insert into pengguna values {str(pengguna)}", )
                        c.execute(f"insert into kurir values {str(kurir)}")
                        request.session["user_email"] = email
                        request.session["user_role"] = request.POST["role"]
                        return redirect("authentication:index")
                    # jika sudah ada email terdaftar maka failed
                    except IntegrityError:
                        context["error"] = "email sudah terdaftar"
                        return render(request, 'authentication/register.html', context)
    return render(request, 'authentication/register.html')