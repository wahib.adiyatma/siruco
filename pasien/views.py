from django.shortcuts import render

# Create your views here.
def create_pasien(request):
	return render(request, 'pasien/create-pasien.html')

def read_pasien(request):
	return render(request, 'pasien/read-pasien.html')

def detail_pasien(request):
	return render(request, 'pasien/detail-pasien.html')

def update_pasien(request):
	return render(request, 'pasien/update-pasien.html')

# def delete_pasien(request):
# 	return null;	