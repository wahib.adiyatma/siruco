from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('create-pasien/', views.create_pasien, name="create-pasien"),
    path('read-pasien/', views.read_pasien, name="read-pasien"),
    path('detail-pasien/', views.detail_pasien, name="detail-pasien"),
    path('update-pasien/', views.update_pasien, name="update-pasien"),
    #path('delete-pasien/', views.delete_pasien, name="delete-pasien"),

]
